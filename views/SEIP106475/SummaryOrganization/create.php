<html>
    <head>
        <title>Book Title</title>
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
    </head>
    <body>

        <div class="container">
            <div class="col-md-8">

            </div>
            <div class="col-md-4">

                <form class="form-inline">
                    <input type="text" class="form-control"  placeholder="Search">
                    <button type="submit" class="btn btn-default ">Search</button>
                </form>
            </div>
            <div class="container ">
                <div class="jumbotron">
                    <form action="store.php" method="POST">
                        <fieldset>
                            <label for="title">Organization Name:</label>
                            <input  id="title" type="text" autofocus="autofocus" 
                                    class="form-inline" name="name" placeholder=" Enter Your  Name"
                                    size="40"
                                    tabindex="1" required="required"/>
                            <br/>
                            <label for="title">Summary:</label>
                            <textarea name="summary" rows="10" cols="140"></textarea>
                            <section>
                                <button type="submit" class="btn btn-success">SAVE</button>
                                <button type="submit" class="btn btn-primary">SAVE & Add Again</button>

                                <button type="reset" class="btn btn-info">RESET</button>
                            </section>
                        </fieldset> 
                    </form>

                </div>
            </div>
        </div>
    </body>
</html>